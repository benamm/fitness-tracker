import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

import { User } from './user.model';
import { AuthData } from './auth-data';

@Injectable()
export class AuthService {
  authChange = new Subject<boolean>();
  private user: User;

  constructor(private router: Router) {}

  registerUser(authData: AuthData) {
    this.user = {
      email: authData.email,
      userId: Math.round(Math.random() * 10000).toString()
    };
    this.authSuccessfully();
  }

  login(authData: AuthData) {
    this.user = {
      email: authData.email,
      userId: Math.round(Math.random() * 10000).toString()
    };
    localStorage.setItem('currentUser', this.user.email);
    this.authSuccessfully();
  }

  logout() {
    this.user = null;
    this.authChange.next(false);
    this.router.navigate(['/']);
  }

  getUser() {
    // return { ...this.user };
    return localStorage.getItem('currentUser');
  }

  isAuth() {
    // return this.user != null;
    const auth = localStorage.getItem('currentUser') != null;
    return auth;
  }

  authSuccessfully() {
    this.authChange.next(true);
    this.router.navigate(['/training']);
  }
}
